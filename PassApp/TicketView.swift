//
//  TicketView.swift
//  
//
//  Created by Asar Sunny on 16/12/2018.
//

import UIKit

class TicketView: UIView {

    @IBOutlet var ticketType: UILabel!
    @IBOutlet var ticketPrice: UILabel!
    @IBOutlet var buyButton: UIButton!
    
}
