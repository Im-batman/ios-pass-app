//
//  Event.swift
//  PassApp
//
//  Created by Asar Sunny on 10/12/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import Foundation
import UIKit


struct Event{
    
    public private(set) var eid: String
    public private(set) var orid: String
    public private(set) var catid: String
    public private(set) var type: String
    public private(set) var name: String
    public private(set) var description: String
    public private(set) var eventImage: String?
    public private(set) var city: String
    public private(set) var feee: String
    public private(set) var event_times: String?
    public private(set) var start_time: String?
    public private(set) var end_time: String?
    
    
    
    
}
