//
//  User.swift
//  PassApp
//
//  Created by Asar Sunny on 10/12/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import Foundation

class UserData {

    static let instance = UserData()
    
    public private(set) var cid = ""
    public private(set) var email = ""
    public private(set) var username = ""
    public private(set) var imageUrl = ""
    
    
    
    func setUseraData(cid:String, email:String,username:String, image:String){
        self.cid = cid
        self.email = email
        self.username = username
        self.imageUrl = image
    }
    
    
    func clearUserData(){
        self.cid = ""
        self.email = ""
        self.username = ""
        self.imageUrl = ""    }
    
    
    
}
