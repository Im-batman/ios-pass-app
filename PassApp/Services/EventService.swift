//
//  EventService.swift
//  PassApp
//
//  Created by Asar Sunny on 10/12/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class EventService{
    static let instance = EventService()
    
   
    
    
    //Get all the events
    func getAllEvents(completion:@escaping (_ events:[Event])->Void){
         var Events = [Event]()
        
        Alamofire.request(GET_ALL_EVENTS, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: BASE_HEADER).responseJSON { (response) in
           
            if response.error == nil{
               let data = response.data
                if let json  = try? JSON(data: data!){
                    guard let eventArray = json["events"]["data"].array else {return}
                    print(eventArray)
                    
                  
                   for item in eventArray{
                        let eid =  item["eid"].stringValue
                        let orid =  item["orid"].stringValue
                        let catid =  item["catid"].stringValue
                        let name =  item["name"].stringValue
                        let description =  item["description"].stringValue
                        let type =  item["type"].stringValue
                        let imageUrl =  item["imageUrl"].stringValue
                        let city =  item["city"].stringValue
                        let fee =  item["fee"].stringValue
                        let eventTimes =  item["event_times"].stringValue
                        let start_time =  item["start_time"].stringValue
                        let end_time =  item["end_time"].stringValue
                    
                    
                    
                    let event = Event(eid: eid, orid: orid, catid: catid, type: type, name: name, description: description, eventImage: imageUrl, city: city, feee: fee, event_times: eventTimes, start_time: start_time, end_time: end_time)
                    
                    Events.append(event)

                        
                    }
                        
                    
    
                    completion(Events)
                    print("Events in the system include ------ \(Events)")
                    
                }
                
            }else{
                debugPrint("\(response.error?.localizedDescription)")
            }
           
            
            
        }
        
        
    }
    
    
    func getEventImage(imageUrl:String, completion:@escaping (_ image:UIImage?)->()){
        Alamofire.request(imageUrl).responseData { (response) in
            if response.error == nil{
                if let data = response.data{
                   completion(UIImage(data: data))
                }
            }
        }
        
        
    
    }
    
    
    
    
    
}
