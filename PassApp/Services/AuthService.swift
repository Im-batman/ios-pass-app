//
//  AuthService.swift
//  PassApp
//
//  Created by Asar Sunny on 07/12/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


class AuthService{
    
    static let instance = AuthService()  //make a singleton to be used throughOut the app
    let defaults = UserDefaults.standard  // an instance of default storage for simple values
    
    var userEmail: String{
        get{
            return defaults.value(forKey: USER_EMAIL ) as! String
        }
        set{
            defaults.set(newValue, forKey: USER_EMAIL)
        }
    
    }
    
    
    var token: String{
        get{
            return defaults.value(forKey: USER_TOKEN) as! String
        }
        set{
            defaults.set(newValue, forKey: USER_TOKEN)
        }
    }
    
    
    var isLoggedIn:Bool{
        get{
            return defaults.bool(forKey: IS_LOGGED_IN)
        }
        set{
            defaults.set(newValue, forKey: IS_LOGGED_IN)
        }
    }
    
    
    
    //register the user
    
    func registerUser(withEmail email: String,andUserName username:String,andPassword password:String,completion:@escaping (_ Completion:Bool)->()){
        let lowerEmail = email.lowercased()
        let body = [
            "email": lowerEmail,
            "password":password,
            "username": username
        ]
        
        
        Alamofire.request(REGISTER_USER, method: .post, parameters: body, encoding: JSONEncoding.default, headers: BASE_HEADER).responseJSON { (response) in
            if response.error == nil{
                completion(true)
                print(response.data!)
            }else{
                completion(false)
                debugPrint("something blocked register...")
            }
        }
        
        
    }
    
    func loginUser(withEmail email: String, andPassword password:String, Completion:@escaping (_ Success:Bool)->()){
        
        let lowerEmail = email.lowercased()
        let body = [
            "email":lowerEmail,
            "password":password
        ]
        
        Alamofire.request(LOGIN_USER, method: .post, parameters: body, encoding: JSONEncoding.default, headers: BASE_HEADER).responseJSON { (response) in
            if response.error == nil{
                let data = response.data
                if  let json = try? JSON(data: data!){
                    print(json)
                    
                    let cid = json["customer"]["cid"].stringValue
                    let customerUsername = json["customer"]["username"].stringValue
                    let customerEmail = json["customer"]["email"].stringValue
                    let customerToken = json["customer"]["api_token"].stringValue
                    let customerImage = json["customer"]["imageUrl"].stringValue
                    UserData.instance.setUseraData(cid: cid, email: customerEmail, username: customerUsername, image: customerImage)
                    
                    self.userEmail = customerEmail
                    self.token = customerToken
                    
                    
                }
                
                self.isLoggedIn = true
                Completion(true)
                
            }else{
                debugPrint("something blocked login \(response.error?.localizedDescription)")
            }
        }
        
        
        
    }
    
    
    
    
    
    func logOutUser(){
        self.userEmail = ""
        self.token = ""
        self.isLoggedIn = false
        UserData.instance.clearUserData()
    }
    
    
}
