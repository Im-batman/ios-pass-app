//
//  RoundedImage.swift
//  PassApp
//
//  Created by Asar Sunny on 26/12/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit

class RoundedImage: UIImageView {

    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupImage()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setupImage()
    }
    
    
    
    
    func setupImage(){
        self.layer.cornerRadius = self.layer.frame.width/2
        clipsToBounds = true
        
    }
    
}
