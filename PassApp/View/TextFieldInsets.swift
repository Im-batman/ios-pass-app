//
//  TextFieldInsets.swift
//  PassApp
//
//  Created by Asar Sunny on 07/12/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit

class TextFieldInsets: UITextField {
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }

    private let padding = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    
    func setupView(){
        let placeHolder = NSAttributedString(string: self.placeholder!, attributes: [NSAttributedStringKey.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)])
        self.attributedPlaceholder = placeHolder
        
        
    }
    
    

}
