//
//  EventCell.swift
//  PassApp
//
//  Created by Asar Sunny on 08/12/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit

class EventCell: UITableViewCell {

    //outlets
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var feeLabel: UILabel!
    
    
//    func setupView(){
//        cellView.layer.shadowRadius = 5
//        cellView.layer.shadowOpacity = 0.75
//        cellView.layer.shadowColor = #colorLiteral(red: 0.3098039329, green: 0.01568627544, blue: 0.1294117719, alpha: 1)
//    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    
    @IBAction func shareBtnPressed(_ sender: Any) {
        let share = UIActivityViewController(activityItems: [title.text], applicationActivities: nil)
        
    }
    
   
    func configureCell(withmage image:UIImage, andTitle title:String, andCity city:String,fee:String, andDate date:String){

        self.bgImage.image = image
        self.title.text = title
        self.cityLabel.text = city
        self.dateLabel.text = date
        self.feeLabel.text = "N\(fee)"
        
//        setupView()
    }
 
 

    

}
