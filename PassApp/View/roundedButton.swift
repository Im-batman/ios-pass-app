//
//  roundedButton.swift
//  PassApp
//
//  Created by Asar Sunny on 07/12/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit
@IBDesignable
class roundedButton: UIButton {

   @IBInspectable var cornerRadius:CGFloat = 5.0{
        didSet{
             self.layer.cornerRadius = cornerRadius
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpView()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setUpView()
    }
    
    
    func setUpView(){
          self.layer.cornerRadius = cornerRadius
    }
    

}
