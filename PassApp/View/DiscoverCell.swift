//
//  DiscoverCell.swift
//  PassApp
//
//  Created by Asar Sunny on 11/12/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit



class DiscoverCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var fee: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var date: UILabel!
    
    
    
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.layer.cornerRadius = 15.0
        self.layer.shadowRadius = 2
        self.layer.shadowOpacity = 0.8
        self.layer.shadowOffset = CGSize(width: 5, height: 10)
        self.layer.shadowColor = #colorLiteral(red: 0.3176470697, green: 0.07450980693, blue: 0.02745098062, alpha: 1)
        clipsToBounds = true
    }
    
    
    func configureCell(withImage:UIImage,name:String,city:String,date:String,fee:String){
        
        self.image.image = withImage
        self.name.text = name
        self.city.text = city
        self.fee.text = fee
        self.date.text = date
    }
    
    
    
}
