//
//  RoundedConerView.swift
//  PassApp
//
//  Created by Asar Sunny on 08/12/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit
@IBDesignable
class RoundedConerView: UIView {

    @IBInspectable var cornerRadius:CGFloat = 3.0 {
        didSet{
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    override func prepareForInterfaceBuilder() {
        setupView()
    }
    
    
    func setupView(){
        self.layer.cornerRadius = cornerRadius
//        self.layer.shadowOffset = CGSize(width: 5, height: 10)
        self.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        self.layer.shadowRadius = 5
        self.layer.shadowOpacity = 0.8
        
        self.clipsToBounds = false
        
        
    
    }
   
    
    
    
    
    

}
