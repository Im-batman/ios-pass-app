//
//  Constants.swift
//  PassApp
//
//  Created by Asar Sunny on 07/12/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import Foundation


//Auth Values
let USER_EMAIL = "userEmail"
let IS_LOGGED_IN = "isLoggedIn"
let USER_TOKEN =         "token"


let BASE_HEADER = [
  "Content-Type" : "Application/json; charset=utf-8"
]




//Urls

//let BASE_URL = "http://127.0.0.1:8000/api/"
let BASE_URL = "http://pass.gurus.ng/api/"

let REGISTER_USER = "\(BASE_URL)user/register"
let LOGIN_USER = "\(BASE_URL)user/login"

let GET_ALL_EVENTS = "\(BASE_URL)user/get-all-events"





