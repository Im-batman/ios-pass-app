//
//  LoginVC.swift
//  PassApp
//
//  Created by Asar Sunny on 07/12/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {
//outlets
    @IBOutlet weak var emailField: TextFieldInsets!
    @IBOutlet weak var passwordField: TextFieldInsets!
    @IBOutlet weak var loginBtn: roundedButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var loadingStack: UIStackView!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadingStack.isHidden = true
        if AuthService.instance.isLoggedIn{
           
            
           dismiss(animated: true, completion: nil)
            let tabs = storyboard?.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
            present(tabs, animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginBtnPressed(_ sender: Any) {
        guard let email = emailField.text, emailField.text != "" else {return}
        guard let password = passwordField.text, passwordField.text != "" else {return}
        
        loadingStack.isHidden = false
        activityIndicator.startAnimating()
        AuthService.instance.loginUser(withEmail: email, andPassword: password) { (success) in
            if success{
                print("login worked")
                print("this is data from user model --- \(UserData.instance.username, UserData.instance.cid)")
                
               self.activityIndicator.stopAnimating()
                self.loadingStack.isHidden = true
              
                let tabs = self.storyboard?.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
                self.present(tabs, animated: true, completion: nil)
                
            }
        }
        
        
        
        
        
    }
    
    
}
