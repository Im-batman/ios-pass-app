//
//  FirstViewController.swift
//  PassApp
//
//  Created by Asar Sunny on 07/12/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit

class DiscoverVC: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        
//        
//        print("this is the userEmail from defaults \(AuthService.instance.userEmail, AuthService.instance.token, AuthService.instance.isLoggedIn)")
//        print("this is the username from userData \(UserData.instance.username)")
//        
//        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        collectionView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension DiscoverVC: UICollectionViewDelegate,UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "discoverCell", for: indexPath) as! DiscoverCell
        let image = UIImage(named:"party_girl")
        cell.configureCell(withImage: image!, name: "party 1", city: "Abuja", date: "19th", fee: "N3000")
        return cell
    }
    
}

