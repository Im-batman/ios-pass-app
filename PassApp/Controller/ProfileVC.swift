//
//  SecondViewController.swift
//  PassApp
//
//  Created by Asar Sunny on 07/12/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {
//outlets
    
    @IBOutlet weak var profileImage: RoundedImage!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       print("entering profile page")
        print("\(AuthService.instance.userEmail)")
        
        
        
        usernameLabel.text = UserData.instance.username
        emailLabel.text = AuthService.instance.userEmail
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func logOutBtnPressed(_ sender: Any) {
        let action = UIAlertController(title:"Log Out", message: "Are you sure?", preferredStyle: .actionSheet)
        
        let logout = UIAlertAction(title: "LogOut", style: .destructive) { (action) in
            AuthService.instance.logOutUser()
            
            let rootVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let delegate = UIApplication.shared.delegate as! AppDelegate
            delegate.window?.makeKeyAndVisible()
            delegate.window?.rootViewController = rootVC
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
//            self.dismiss(animated: true, completion: nil)
        }
        
        action.addAction(logout)
        action.addAction(cancel)
        
        present(action, animated: true, completion: nil)
        
        
        
        
    }
    
    
    
}

