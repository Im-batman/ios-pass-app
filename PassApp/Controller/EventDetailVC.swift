//
//  EventDetailVC.swift
//  PassApp
//
//  Created by Asar Sunny on 12/12/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit

class EventDetailVC: UIViewController,UIScrollViewDelegate {
//outlets
    
    @IBOutlet weak var ticketScrollView: UIScrollView!
    @IBOutlet weak var eventImage: UIImageView!
    @IBOutlet weak var eventTitle: UILabel!
    @IBOutlet weak var eventDate: UILabel!
    @IBOutlet weak var eventCity: UILabel!
    @IBOutlet weak var eventDescription: UILabel!
    @IBOutlet weak var startingPrice: UILabel!
    
    @IBOutlet weak var pageIndcator: UIPageControl!
    
    
    let ticket1 = ["type":"regular","price":"1500"]
     let ticket2 = ["type":"VIP","price":"3000"]
     let ticket3 = ["type":"Gold","price":"5,000"]
     let ticket4 = ["type":"Yes Boss","price":"10,000"]
    
    var ticketsArray = [Dictionary<String,String>]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
         ticketsArray = [ticket1,ticket2,ticket3,ticket4]
        ticketScrollView.isPagingEnabled = true
        ticketScrollView.contentSize = CGSize(width: self.view.bounds.width * CGFloat(ticketsArray.count), height: 250)
        ticketScrollView.showsHorizontalScrollIndicator = false
        loadTickets()
        ticketScrollView.delegate = self
    }
    
    
    func loadTickets(){
        for (index,ticket) in ticketsArray.enumerated(){
            let ticketView = Bundle.main.loadNibNamed("tickets", owner: self, options: nil)?.first as! TicketView
               ticketView.ticketType.text = ticket["type"]
            ticketView.ticketPrice.text = ticket["price"]
            
            ticketScrollView.addSubview(ticketView)
            ticketView.frame.size.width = self.view.bounds.size.width
            ticketView.frame.origin.x = CGFloat(index) * self.view.bounds.size.width
            
            
            
        }
    }
    
    func setupData(image:UIImage,title:String,description:String,city:String,price:String) {
        self.eventImage.image = image
        self.eventTitle.text = title
        self.eventDescription.text = description
        self.eventCity.text = city
        self.startingPrice.text = price
    }
    
    
    @IBAction func closeBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let page = scrollView.contentOffset.x / scrollView.frame.size.width
        pageIndcator.currentPage = Int(page)
    }
    

    

}
