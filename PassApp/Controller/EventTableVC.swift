//
//  EventVC.swift
//  PassApp
//
//  Created by Asar Sunny on 08/12/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit
import Lottie

class EventTableVC: UIViewController {
    var EventsArray =  [Event]()
    var picture:UIImage?
    
    
    @IBOutlet weak var loadingView: LOTAnimationView!
    //outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        tableView.isHidden = true
        loader()

        fetchdata()
    }
    
    
    
    
    func loader(){
        loadingView.setAnimation(named: "floral_loading_animation")
        loadingView.loopAnimation = true
        loadingView.play()
    }
    
    
    func fetchdata(){
        
        EventService.instance.getAllEvents { (returnedEvents) in
            self.EventsArray = returnedEvents
            self.loadingView.stop()
            
            self.activityIndicator.startAnimating()
            self.activityIndicator.isHidden = true
            self.tableView.isHidden = false
            self.tableView.reloadData()
        }
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    @IBAction func shareBtnPressed(_ sender: Any) {
//        let indexpath =  tableView.indexPathsForSelectedRows!
//        let title = "hello world"
//        let activity = UIActivityViewController(activityItems: [title, applicationActivities:nil)
//        
//        activity.pop
//        
        
        
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

extension EventTableVC: UITableViewDelegate,UITableViewDataSource{

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return EventsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "eventCell", for: indexPath) as? EventCell else {return UITableViewCell()}

             let event = EventsArray[indexPath.row]
        
        
        EventService.instance.getEventImage(imageUrl: event.eventImage!) { (image) in
                self.picture = image
            cell.configureCell(withmage:image!, andTitle: event.name, andCity: event.city, fee: event.feee, andDate: "19th Dec")
        }
        
            return cell
      
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailSegue"{
             let indexPath = tableView.indexPathForSelectedRow!
            let event = EventsArray[indexPath.row]
           
            EventService.instance.getEventImage(imageUrl: event.eventImage!, completion: { (image) in
                
                let DetailVC = segue.destination as! EventDetailVC
                DetailVC.setupData(image: image!, title: event.name, description: event.description, city: event.city, price: event.feee)
                
                
                
            })
            
            
            
    
        }
        
        
    }
    
    
    
}
