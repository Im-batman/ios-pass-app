//
//  RegisterVC.swift
//  PassApp
//
//  Created by Asar Sunny on 07/12/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit

class RegisterVC: UIViewController {
//outlets
    @IBOutlet weak var userNameField: TextFieldInsets!
    @IBOutlet weak var emailField: TextFieldInsets!
    @IBOutlet weak var passwordField: TextFieldInsets!
    @IBOutlet weak var signUpBtn: roundedButton!
    @IBOutlet weak var loadingStack: UIStackView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        loadingStack.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func closeBtnPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func signUpBtnPressed(_ sender: Any) {
        print("register button has been pressed")
        signUpBtn.isEnabled = false
        loadingStack.isHidden = false
        activityIndicator.startAnimating()
    
        guard let username = userNameField.text, userNameField.text != "" else {return}
        guard let email = emailField.text, emailField.text != "" else {return}
        guard let password = passwordField.text, passwordField.text != "" else {return}
        
        AuthService.instance.registerUser(withEmail: email, andUserName: username, andPassword: password) { (success) in
            if success{
                print("User registered fine!!")
                AuthService.instance.loginUser(withEmail: email, andPassword: password, Completion: { (success) in
                    if success{
                        self.activityIndicator.stopAnimating()
                        self.loadingStack.isHidden = true
                        print("token is = \(AuthService.instance.token)")
                        self.dismiss(animated: true, completion: nil)
                    }
                })
            }
        }
        
       
        
        
    }
    
   
}
